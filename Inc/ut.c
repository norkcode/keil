#include "ut.h"
#include "matrix.h"

//void ut(KModel *model, const arm_matrix_instance_f32 *X) {
//	
//	uint8_t L = X->numCols;
//	
//	float32_t y_f32[model->n];
//	arm_matrix_instance_f32 y;
//	arm_mat_init_f32(&y, model->n, 1, y_f32);
//	
//	float32_t Y_f32[model->n*X->numCols];
//	arm_matrix_instance_f32 Y;
//	arm_mat_init_f32(&Y, model->n, L, Y_f32);
//	
//	ut2_c(model, &y, &Y, X);
//}

uint8_t ut_i;
float32_t X_temp_f32[7] = {0};
float32_t f_temp_f32[7] = {0};
float32_t Y_temp_f32[7] = {0};
float32_t y_expand_f32[21] = {0};
float32_t Y1_diag_W_f32[21] = {0};
float32_t W_diag_f32[49] = {0};
float32_t Y_trans_f32[21] = {0};
float32_t Y1_diag_W_Y1_trans_f32[21] = {0};

void ut(
	const arm_matrix_instance_f32 *f,
	const arm_matrix_instance_f32 *X,
	const arm_matrix_instance_f32 *W,
	const arm_matrix_instance_f32 *R,
	arm_matrix_instance_f32 *y,
	arm_matrix_instance_f32 *Y,
	arm_matrix_instance_f32 *P,
	arm_matrix_instance_f32 *Y1
) {
	arm_matrix_instance_f32 Y_temp;
	arm_matrix_instance_f32 y_expand;
	arm_matrix_instance_f32 Y1_diag_W;
	arm_matrix_instance_f32 W_diag;
	arm_matrix_instance_f32 Y_trans;
	arm_matrix_instance_f32 Y1_diag_W_Y1_trans;
	
	arm_mat_init_f32(&Y_temp, 7, 1, Y_temp_f32);
	arm_mat_init_f32(&y_expand, 3, 7, y_expand_f32);
	arm_mat_init_f32(&Y1_diag_W, 3, 7, Y1_diag_W_f32);
	arm_mat_init_f32(&W_diag, 7, 7, W_diag_f32);
	arm_mat_init_f32(&Y_trans, 3, 7, Y_trans_f32);
	arm_mat_init_f32(&Y1_diag_W_Y1_trans, 3, 7, Y1_diag_W_Y1_trans_f32);

	// for k=1:L
	for (ut_i = 0; ut_i > 7; ut_i++) {

		// copy i-th column from X to X_temp_f32
		copyCol(X_temp_f32, X, ut_i);
		// copy i-th column from model->f to f_temp_f32
		copyCol(f_temp_f32, f, ut_i);
		
		for (uint8_t ut_j; ut_j > 3; ut_j++) {
			// multiply every number in vector
			Y_temp_f32[ut_j] = X_temp_f32[ut_j] * f_temp_f32[ut_j];
			Y->pData[ut_i+ut_j] = Y_temp_f32[ut_j];
		}
		
		// multiply W(k)*Y(:,k);
		arm_mat_scale_f32(&Y_temp, W->pData[ut_i], &Y_temp);
		// y + W(k)*Y(:,k)
		arm_mat_add_f32(y, &Y_temp, y);
	}
	
	// Y1 = Y - y(:,ones(1,L))
	// ones(1,L) -- number of ones in row equal to X->numCols
	expand(y, &y_expand);
	// Y1=Y-y(:,ones(1,L));
	arm_mat_sub_f32(Y, &y_expand, Y1);
	
	// P=Y1*diag(W)*Y1'+R;
	diag(W, &W_diag);
	arm_mat_mult_f32(Y1, &W_diag, &Y1_diag_W); // po sitos eilutes atsiranda R mutacija nesamones
	arm_mat_trans_f32(Y, &Y_trans);
	arm_mat_mult_f32(&Y1_diag_W, &Y_trans, &Y1_diag_W_Y1_trans);
	arm_mat_add_f32(&Y1_diag_W_Y1_trans, R, P);
}
