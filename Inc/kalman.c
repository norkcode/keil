#include "kalman.h"
#include "message.h"
#define ARM_MATH_CM4

const float32_t h_f32[3] = {
	0, 0, 1
};

// -0.5*dT*dT;
// float32_t dT1 = -0.005;
// dT = 0.016
const float32_t f_f32[9] = {
	1, 1, -0.000128,
	0, 1, 0.016,
	0, 0, 1
};

void kalman_init(UART_HandleTypeDef *huart2, KModel *m) {
	arm_status status; 
	// assign usart
	m->huart2 = huart2;
	
	// 3 states
	m->n = 3;
	// 3 measurements
	m->ss = 1;
	// process std
	m->q = 0.1;
	// measurement std
	m->r = 0.1;
	// sample rate
	m->dT = 0.1;
	
	arm_mat_init_f32(&m->f, 3, 3, (float32_t *)f_f32);
		
	arm_mat_init_f32(&m->h, 3, 3, (float32_t *) h_f32);
	
	// define covariance of process
	m->Q_f32[0] = 1;
	m->Q_f32[4] = 1;
	m->Q_f32[8] = 1;

	arm_mat_init_f32(&m->Q, 3, 3, m->Q_f32);
	//eye(&m->Q, 9);
	status = arm_mat_scale_f32(&m->Q, m->q*m->q, &m->Q);

	// define covariance of measurement
	m->R_f32[0] = 1;
	// eye end
	//eye(&m->R, 1);
	arm_mat_init_f32(&m->R, 1, 1, m->R_f32);
	// m->r*m->r = 0.1 * 0.1 = 0.01
	status = arm_mat_scale_f32(&m->R, 0.01, &m->R);
	
	m->nx = m->n;
	m->nz = m->ss;
	
	// define m
	arm_mat_init_f32(&m->m, 3, 1, m->m_f32);
	
	// define P
	m->P_f32[0] = 1;
	m->P_f32[4] = 1;
	m->P_f32[8] = 1;
	arm_mat_init_f32(&m->P, 3, 3, m->P_f32);
	status = arm_mat_scale_f32(&m->P, m->q, &m->P);
	//eye(&m->P, m->n);
	
	// define kappa
	m->kappa = 0;
	
	// define W
	for (uint8_t i = 0; i < 2*m->n; i++) {
		m->W_f32[i + 1] = 0.5f / (m->kappa + m->nx);
	}
	m->W_f32[0] = m->kappa / (m->kappa + m->nx);
	arm_mat_init_f32(&m->W, 7, 1, m->W_f32);
	
	// define c
	arm_sqrt_f32(m->kappa + m->nx, &m->c);
	
	// define MM
	arm_mat_init_f32(&m->MM, m->nx, 1, m->MM_f32);
	
	// define PP
	//float32_t PP_f32[m->nx*m->nx];
	arm_mat_init_f32(&m->PP, m->nx, m->nx, m->PP_f32);
}

void kalman_process(KModel *model, float32_t sample) {
	
	// float declarations
	// model->m.numRows + 2 * model->P.numCols * model->m.numRows;
	float32_t X_f32[21] = {0};
	
	
	float32_t m_hat_f32[3] = {0}; // n=3
	float32_t X_hat_f32[3] = {0}; // n=3
	float32_t P_hat_f32[9] = {0}; // nxn = 3x3
	float32_t X_dev_f32[21] = {0}; // model->m.numRows + 2 * model->P.numCols * model->m.numRows
	float32_t z_hat_f32[1] = {0};
	float32_t Z_hat_f32[7] = {0}; // 3x19 do not know how to derive 19
	float32_t P_zz_f32[1] = {0};
	float32_t Z_dev_f32[7] = {0}; // 1x7
	float32_t Z_dev_transpose_f32[7] = {0}; // 3x19
	float32_t W_diag_f32[49] = {0};
	float32_t S_f32[1] = {0};
	float32_t S_inv_f32[1] = {0};
	float32_t X_dev_W_diag_f32[21] = {0};
	float32_t P_xz_f32[3] = {0};
	float32_t K_f32[3] = {0};
	float32_t K_transpose_f32[3] = {0};
	float32_t Z_r_f32[1] = {0};
	float32_t Z_r_z_hat_f32[1] = {0};
	float32_t K_Z_r_z_hat_f32[1] = {0};

	Z_r_f32[0] = sample;
	
	// sample
	arm_matrix_instance_f32 Z_r; arm_mat_init_f32(&Z_r, 1, 1, Z_r_f32);

	// matrix instances
	// first ut part
	arm_matrix_instance_f32 X; arm_mat_init_f32(&X, 3, 7, X_f32);
	arm_matrix_instance_f32 m_hat; arm_mat_init_f32(&m_hat, 3, 1, m_hat_f32);
		

  // 1 + 2 * model->P.numCols = 19
	arm_matrix_instance_f32 X_hat; arm_mat_init_f32(&X_hat, 3, 7, X_hat_f32);
	arm_matrix_instance_f32 P_hat; arm_mat_init_f32(&P_hat, 3, 3, P_hat_f32);
	arm_matrix_instance_f32 X_dev; arm_mat_init_f32(&X_dev, 3, 7, X_dev_f32);
	
	// second ut part
	arm_matrix_instance_f32 z_hat; arm_mat_init_f32(&z_hat, 1, 1, z_hat_f32);
	arm_matrix_instance_f32 Z_hat; arm_mat_init_f32(&Z_hat, 1, 7, Z_hat_f32);
	arm_matrix_instance_f32 P_zz; arm_mat_init_f32(&P_zz, 1, 1, P_zz_f32);
	arm_matrix_instance_f32 Z_dev; arm_mat_init_f32(&Z_dev, 1, 7, Z_dev_f32);

	// helpers
	arm_matrix_instance_f32 W_diag; arm_mat_init_f32(&W_diag, model->n, model->n, W_diag_f32);
	arm_matrix_instance_f32 S; arm_mat_init_f32(&S, model->ss, model->ss, S_f32);
	arm_matrix_instance_f32 X_dev_W_diag; arm_mat_init_f32(&X_dev_W_diag, model->n, 7, X_dev_W_diag_f32);
	arm_matrix_instance_f32 Z_dev_transpose; arm_mat_init_f32(&Z_dev_transpose, 7, model->ss, Z_dev_transpose_f32);
	arm_matrix_instance_f32 P_xz; arm_mat_init_f32(&P_xz, model->n, model->n, P_xz_f32);
	arm_matrix_instance_f32 K; arm_mat_init_f32(&K, model->n, model->n, K_f32);
	arm_matrix_instance_f32 K_transpose; arm_mat_init_f32(&K_transpose, model->n, model->n, K_transpose_f32);
	arm_matrix_instance_f32 S_inv; arm_mat_init_f32(&S_inv, model->ss, model->ss, S_inv_f32);
	arm_matrix_instance_f32 Z_r_z_hat; arm_mat_init_f32(&Z_r_z_hat, model->ss, 1, Z_r_z_hat_f32); // Z_r - z_hat
	arm_matrix_instance_f32 K_Z_r_z_hat; arm_mat_init_f32(&K_Z_r_z_hat, model->n, 1, K_Z_r_z_hat_f32); // K *(Z_r - z_hat)
	//arm_matrix_instance_f32 P_xz_K_transpose; arm_mat_init_f32(&P_xz_K_transpose, model->n, model->n, P_xz_K_transpose_f32); // P_xz * K'
	
	// process
	sigma_points(&model->m, &model->P, model->c, &X);
	ut(&model->f, &X, &model->W, &model->Q, &m_hat, &X_hat, &P_hat, &X_dev);
	
	ut(&model->h, &X_hat, &model->W, &model->R, &z_hat, &Z_hat, &P_zz, &Z_dev);
	
	// P_xz = X_dev * diag(W) * Z_dev';
	diag(&model->W, &W_diag);
	arm_mat_mult_f32(&X_dev, &W_diag, &X_dev_W_diag);
	arm_mat_trans_f32(&Z_dev, &Z_dev_transpose);
	arm_mat_mult_f32(&X_dev_W_diag, &Z_dev_transpose, &P_xz);
	
	// S = R + P_zz;
	arm_mat_add_f32(&model->R, &P_zz, &S);
	
	// K = P_xz / S;
	arm_mat_inverse_f32(&S, &S_inv);
	arm_mat_mult_f32(&P_xz, &S_inv, &K);
	
	// m = m_hat + K *(Z_r - z_hat);
	arm_mat_sub_f32(&Z_r, &z_hat, &Z_r_z_hat);
	arm_mat_mult_f32(&K, &Z_r_z_hat, &K_Z_r_z_hat);
	arm_mat_add_f32(&m_hat, &K_Z_r_z_hat, &model->MM);
	
	// P = P_hat - P_xz * K';
	//arm_mat_mult_f32(&P_xz, &K_transpose, &P_xz_K_transpose);
	arm_mat_mult_f32(&P_xz, &K_transpose, &P_xz);
	//arm_mat_sub_f32(&P_hat, &P_xz_K_transpose, &model->PP);
	arm_mat_sub_f32(&P_hat, &P_xz, &model->PP);
	
}


