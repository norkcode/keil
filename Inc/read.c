#include "read.h"
#include "mpu.h"
#include "transport/transport.h"



void readGyro(SPI_HandleTypeDef hspi1, int16_t * data)
{
	uint8_t data_read[7];
	uint8_t data_write[6];

	data_write[0] = 0x43 | READ_FLAG;
	data_write[1] = 0x44 | READ_FLAG;
	data_write[2] = 0x45 | READ_FLAG;
	data_write[3] = 0x46 | READ_FLAG;
	data_write[4] = 0x47 | READ_FLAG;
	data_write[5] = 0x48 | READ_FLAG;

	transportEnable();

	// All axes
	HAL_SPI_TransmitReceive(&hspi1, data_write, data_read, 7, 200);

	transportDisable();

	int16_t data_read_x = ((int16_t) data_read[1] << 8) | data_read[2];
	int16_t data_read_y = ((int16_t) data_read[3] << 8) | data_read[4];
	int16_t data_read_z = ((int16_t) data_read[5] << 8) | data_read[6];

	// scale
	data_read_x = data_read_x;
	data_read_z = data_read_z;
	data_read_y = data_read_y;

	// output
	data[0] = data_read_x;
	data[1] = data_read_y;
	data[2] = data_read_z;
}

uint8_t readByte(SPI_HandleTypeDef hspi1, uint8_t address)
{
	uint8_t data_read[2];
	uint8_t data_write[1];

	data_write[0] = address;

	transportEnable();

	HAL_SPI_TransmitReceive(&hspi1, data_write, data_read, 2, 100);

	transportDisable();

	// first bit is shit
	return data_read[1];
}

void readBytes(SPI_HandleTypeDef hspi1, uint8_t address, uint8_t *data_read, int8_t count)
{
	uint8_t data_write[1];
	data_write[0] = address;

	transportEnable();

	HAL_SPI_TransmitReceive(&hspi1, data_write, data_read, count+1, 100);

	transportDisable();
}

uint8_t readFifoPackageCount(SPI_HandleTypeDef hspi1)
{
	uint8_t data[3];
	int16_t packet_count;
	
	data[1] = readByte(hspi1, MPUREG_FIFO_COUNTH | READ_FLAG);
	data[0] = readByte(hspi1, MPUREG_FIFO_COUNTL | READ_FLAG);
	
	packet_count = ((uint16_t)data[1] << 8) | data[0];
	// How many sets of full gyro and accelerometer data for averaging
  packet_count = packet_count/12;
	
	return packet_count;
}
