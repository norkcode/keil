#include "message.h"

void debugMessage(UART_HandleTypeDef *huart2, char *message) {
	uint8_t tx[strlen(message)];

	size_t i = 0;
	while (message[i]) {
		tx[i] = message[i];
		i += 1;
	}

	HAL_UART_Transmit(huart2, tx, strlen(message), 100);
}
