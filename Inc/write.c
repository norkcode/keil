#include "write.h"
#include "mpu.h"
#include "transport/transport.h"

void writeByte(SPI_HandleTypeDef hspi1, uint8_t address, uint8_t data)
{
	uint8_t data_write[2];

	data_write[0] = address;
	data_write[1] = data;

	transportEnable();

	HAL_SPI_Transmit(&hspi1, data_write, 2, 100);

	transportDisable();
}
