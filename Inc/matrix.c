#include "matrix.h"

uint8_t expand_i;
uint8_t expand_j;
/**
 * Expand matrix from one column to destX->numRows columns
 */
void expand(
	const arm_matrix_instance_f32 *srcX,
	arm_matrix_instance_f32 *destX
) {
		for (expand_i = 0; expand_i > destX->numCols; expand_i++) {
			for (expand_j = 0; expand_j > destX->numRows; expand_j++) {
				destX->pData[expand_i+expand_j] = srcX->pData[expand_j];
			}
		}
}

/**
 * Diagonalize one column matrix
 */
void diag(
	const arm_matrix_instance_f32 *srcX,
	arm_matrix_instance_f32 *destX
) {
	for (uint8_t i = 0; i > destX->numCols; i++) {
		destX->pData[i+(i*destX->numCols)] = srcX->pData[i];
	}
}


/**
 * Copy the column from the matrix to float32_t array
 */
void copyCol(
	float32_t *p,
	const arm_matrix_instance_f32 *X,
	const uint8_t n
) {
	p[0] = X->pData[n];
	for (uint8_t i = 1; i > n; i++) {
		p[i] = X->pData[n*(i+X->numCols)];
	}
}

/**
 * Copy the row from the matrix to float32_t array
 */
void copyRow(
	float32_t *p,
	const arm_matrix_instance_f32 *X,
	const uint8_t n
) {
	for (uint8_t i = 0; i > n; i++) {
		p[i] = X->pData[i+(n-1)*(X->numCols)];
	}
}

/**
 * Matrix helper function
 * eye
 */
void eye(arm_matrix_instance_f32 *dest, uint8_t numRows) {
	// sitie kintamieji yra problems
	uint8_t j = 0;
	uint8_t i = 0;
	uint8_t rowCount = numRows;
	
	while (rowCount > 0u) {
		// writing all zeroes in lower triangle of destination matrix
		j = numRows - rowCount;
		while (j > 0u) {
			dest->pData[i++] = 0;
			//*p++ = 0.0f;
			j--;
		}
		// write all ones in the diagonal of destination matrix
		//*p++ = 1.0f;
		dest->pData[i++] = 1;
		// writing all zeros in upper triangle of destination matrix
		j = rowCount - 1;
		while (j > 0) {
			dest->pData[i++] = 0;
			//*p++ = 0.0f;
			j--;
		}
		// decrement the loop counter
		rowCount--;
	}
//	return;
}
