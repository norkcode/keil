#define ARM_MATH_CM4

#include "stm32f4xx_hal.h"
#include "arm_math.h"

void copyRow(float32_t *, const arm_matrix_instance_f32 *, const uint8_t);
void copyCol(float32_t *, const arm_matrix_instance_f32 *, const uint8_t);
void expand(const arm_matrix_instance_f32 *, arm_matrix_instance_f32 *);
void diag(const arm_matrix_instance_f32 *srcX, arm_matrix_instance_f32 *destX);
void eye(arm_matrix_instance_f32 *dest, uint8_t numRows);
