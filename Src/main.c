/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

#include "mpu.h"
#include "util.h"
#include "read.h"
#include "write.h"
#include "kalman.h"
#include "transport/transport.h"
#include "message.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SPI1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/


/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

float a_bias[3];
float g_bias[3];
float y_1;
float x_1;
float z_1;


/**
 *
 */

void readAcc(SPI_HandleTypeDef hspi1, int16_t* data)
{
	//char local_message[80];
	
	uint8_t data_read[12];
	uint8_t data_write[6];

	data_write[0] = 0x3B | READ_FLAG;
	data_write[1] = 0x3C | READ_FLAG;
	data_write[2] = 0x3D | READ_FLAG;
	data_write[3] = 0x3E | READ_FLAG;
	data_write[4] = 0x3F | READ_FLAG;
	data_write[5] = 0x40 | READ_FLAG;

	transportEnable();

	// All axes
	HAL_SPI_TransmitReceive(&hspi1, data_write, data_read, 7, 200);

	transportDisable();

	int16_t data_read_x = ((int16_t)((int16_t) data_read[1] << 8) | data_read[2]);
	int16_t data_read_y = ((int16_t)((int16_t) data_read[3] << 8) | data_read[4]);
	int16_t data_read_z = ((int16_t)((int16_t) data_read[5] << 8) | data_read[6]);

	// scale
	data_read_x = data_read_x;
	data_read_y = data_read_y;
	data_read_z = data_read_z;

	// output
	data[0] = data_read_x;
	data[1] = data_read_y;
	data[2] = data_read_z;
}

void debug(char *msg, int16_t x)
{
	char message[40];
	sprintf(message, "%s: %d\n", msg, x);

	uint8_t tx[strlen(message)];

	size_t i = 0;
	while (message[i]) {
		tx[i] = message[i];
		i += 1;
	}

	HAL_UART_Transmit(&huart2, tx, strlen(message), 100);
}

float diff(float a, float b)
{
	if (a > b) {
		return a - b;
	} else {
		return b - a;
	}
}

void adjustAcc(int16_t * in, float32_t * out) {
	float divider = 32768.0/2.0;
	
	float x_r = in[0] / divider;
	float y_r = in[1] / divider;
	float z_r = in[2] / divider;

	out[0] = x_r - a_bias[0];
	out[1] = y_r - a_bias[1];
	// z axis has different optinion about gravity
	out[2] = z_r - a_bias[2]; 
}


//void debugAcc(float32_t * data)
//{
//	char local_message[50];
//	
////	float divider = 32768.0/2.0;
////	
////	float x_r = data[0] / divider;
////	float y_r = data[1] / divider;
////	float z_r = data[2] / divider;

////	float x = x_r - a_bias[0];
////	float y = y_r - a_bias[1];
////	// z axis has different optinion about gravity
////	float z = z_r - a_bias[2]; 

//	
//	sprintf(local_message, "%.4f,%.4f,%.4f\n", data[0], data[1], data[2]);
//	debugMessage(&huart2, local_message);

//	// accelerometer does not send new line
////	sprintf(local_message, "x: %.2f, xb: %.2f, xc: %.2f\n", x_r, a_bias[0], x);
////	debugMessage(local_message);
////	sprintf(local_message, "y: %.2f, yb: %.2f, yc: %.2f\n", y_r, a_bias[1], y);
////	debugMessage(local_message);
////	sprintf(local_message, "z: %.2f, zb: %.2f, zc: %.2f\n", z_r, a_bias[2], z);
////	debugMessage(local_message);

////	uint8_t tx[strlen(local_message)];

////	size_t i = 0;
////	while (local_message[i]) {
////		tx[i] = local_message[i];
////		i += 1;
////	}

////	HAL_UART_Transmit(&huart2, tx, strlen(local_message), 100);
//}

void debugGyro(int16_t * data)
{
//	char message[40];
	
	float divider = 32768.0/250.0;
	
	float x_r = data[0] / divider;
	float y_r = data[1] / divider;
	float z_r = data[2] / divider;
}

void mpuCalibrate(float * dest1, float * dest2)
{
	// = 131 LSB/degrees/sec
	uint16_t gyrosensitivity  = 131;
	// = 16384 LSB/g
	uint16_t accelsensitivity = 16384;
	// char message buffer
//	char message[80];
	// data buffer
	uint8_t data[12];
	// package count
	int16_t packet_count;
	
	// reset device, reset all registers, clear gyro and accelerometer bias registers
	writeByte(hspi1, MPUREG_PWR_MGMT_1, 0x80);
	HAL_Delay(100);
	
	// get stable time source
	// set clock source to be ppl with x axis gyroscope reference, 2:0=001
	writeByte(hspi1, MPUREG_PWR_MGMT_1, 0x01);
	writeByte(hspi1, MPUREG_PWR_MGMT_2, 0x00);
	HAL_Delay(200);
	
	/*
	 * Configure device for bias calculations
	 */
	
	writeByte(hspi1, MPUREG_INT_ENABLE, 0x00); // Disable all interrupts	
	writeByte(hspi1, MPUREG_FIFO_EN, 0x00); // Disable FIFO	
	writeByte(hspi1, MPUREG_PWR_MGMT_1, 0x00); // Turn on internal clock source
	writeByte(hspi1, MPUREG_I2C_MST_CTRL, 0x00); // Disable I2C master
	writeByte(hspi1, MPUREG_USER_CTRL, 0x00); // Disable FIFO and I2C master modes
	writeByte(hspi1, MPUREG_USER_CTRL, 0x0C); // Reset FIFO and DMP
	HAL_Delay(15);
	
	packet_count = readFifoPackageCount(hspi1);
	
	//sprintf(message, "Sample size before: %d", packet_count);
	//debug(message, 40);

	/*
	 * Configure MPU9250 gyro and accelerometer for bias calculation
	 */

	writeByte(hspi1, MPUREG_CONFIG, 0x01); // Set low-pass filter to 188 Hz
	writeByte(hspi1, MPUREG_SMPLRT_DIV, 0x01); // Set sample rate to 1 kHz
	writeByte(hspi1, MPUREG_GYRO_CONFIG, 0x00); // Set gyro full-scale to 250 degrees per second, maximum sensitivity
	writeByte(hspi1, MPUREG_ACCEL_CONFIG, 0x00); // Set accelerometer full-scale to 2 g, maximum sensitivity, disable all self tests
	

	/* 
	 * Configure FIFO to capture accelerometer and gyro data for bias calculation
	 */

	writeByte(hspi1, MPUREG_USER_CTRL, 0x40); // Enable FIFO
  writeByte(hspi1, MPUREG_FIFO_EN, 0x78); // Enable gyro and accelerometer sensors for FIFO (max size 512 bytes in MPU-9250)
	//writeByte(hspi1, MPUREG_FIFO_EN, 0x08);
	
	HAL_Delay(60);
	
	// Disable gyro and accelerometer sensors for FIFO
	writeByte(hspi1, MPUREG_FIFO_EN, 0x00);
	
	// packages received
	packet_count = readFifoPackageCount(hspi1);
	
	//sprintf(message, "Sample size after: %d\n", packet_count);
	//debugMessage(message);
	uint8_t data_temp[14];
	//readBytes(hspi1, MPUREG_FIFO_R_W | READ_FLAG, data_temp, 1);
	
	int16_t ii = 0;

	int32_t gyro_bias[3] = {0, 0, 0};
	int32_t accel_bias[3] = {0, 0, 0};

	for (ii = 0; ii < packet_count; ii++) {
		int16_t accel_temp[3] = {0, 0, 0};
		int16_t gyro_temp[3] = {0, 0, 0};
		
		readBytes(hspi1, MPUREG_FIFO_R_W | READ_FLAG, data_temp, 12);
		
		// Form signed 16-bit integer for each sample in FIFO
		accel_temp[0] = (int16_t) ( (int16_t) data_temp[1] << 8 | data_temp[2]);
		accel_temp[1] = (int16_t) ( (int16_t) data_temp[3] << 8 | data_temp[4]);
		accel_temp[2] = (int16_t) ( (int16_t) data_temp[5] << 8 | data_temp[6]);

		gyro_temp[0] = (int32_t) ( (int32_t) data_temp[6] << 8 | data_temp[7]);
		gyro_temp[1] = (int32_t) ( (int32_t) data_temp[8] << 8 | data_temp[9]);
		gyro_temp[2] = (int32_t) ( (int32_t) data_temp[10] << 8 | data_temp[11]);
		
		//sprintf(message, "Gtb x: %x, y: %x, z: %x", gyro_temp[0], gyro_temp[1], gyro_temp[2]); debug(message, 40);

		// Sum individual signed 16-bit biases to get accumulated signed 32-bit biases
		accel_bias[0] += (int32_t) accel_temp[0];
		accel_bias[1] += (int32_t) accel_temp[1];
		accel_bias[2] += (int32_t) accel_temp[2];
		
		//sprintf(message, "accel_bias_0: %d, accel_bias_1: %d, accel_bias_2: %d\n", accel_bias[0], accel_bias[1], accel_bias[2]);
		//debugMessage(message);

		gyro_bias[0] += (int32_t) gyro_temp[0];
		gyro_bias[1] += (int32_t) gyro_temp[1];
		gyro_bias[2] += (int32_t) gyro_temp[2];
		
		// delay 1ms
		HAL_Delay(1);
	}

	// Normalize sums to get average count biases
	accel_bias[0] /= (int32_t) packet_count;
	accel_bias[1] /= (int32_t) packet_count;
	accel_bias[2] /= (int32_t) packet_count;
	
	gyro_bias[0]  /= (int32_t) packet_count;
	gyro_bias[1]  /= (int32_t) packet_count;
	gyro_bias[2]  /= (int32_t) packet_count;
	
	if (accel_bias[2] > 0L) {
		accel_bias[2] -= (int32_t) accelsensitivity;
	} else {
	// Remove gravity from the z-axis accelerometer bias calculation
		accel_bias[2] += (int32_t) accelsensitivity;
	}

	// Construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
	// Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format
	data[0] = (-gyro_bias[0]/4  >> 8) & 0xFF;
	// Biases are additive, so change sign on calculated average gyro biases
	data[1] = (-gyro_bias[0]/4)       & 0xFF;
	data[2] = (-gyro_bias[1]/4  >> 8) & 0xFF;
	data[3] = (-gyro_bias[1]/4)       & 0xFF;
	data[4] = (-gyro_bias[2]/4  >> 8) & 0xFF;
	data[5] = (-gyro_bias[2]/4)       & 0xFF;
	
	/// Push gyro biases to hardware registers
	dest1[0] = (float) gyro_bias[0]/(float) gyrosensitivity; // construct gyro bias in deg/s for later manual subtraction
	dest1[1] = (float) gyro_bias[1]/(float) gyrosensitivity;
	dest1[2] = (float) gyro_bias[2]/(float) gyrosensitivity;
	
	// Construct the accelerometer biases for push to the hardware accelerometer bias registers. These registers contain
	// factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold
	// non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature
	// compensation calculations. Accelerometer bias registers expect bias input as 2048 LSB per g, so that
	// the accelerometer biases calculated above must be divided by 8.

	// A place to hold the factory accelerometer trim biases
	int32_t accel_bias_reg[3] = {0, 0, 0};

	// Read factory accelerometer trim values
	readBytes(hspi1, MPUREG_XA_OFFSET_H | READ_FLAG, &data[0], 2);
	accel_bias_reg[0] = (int16_t) ((int16_t)data[0] << 8) | data[1];

	readBytes(hspi1, MPUREG_YA_OFFSET_H | READ_FLAG, &data[0], 2);
	accel_bias_reg[1] = (int16_t) ((int16_t)data[0] << 8) | data[1];

	readBytes(hspi1, MPUREG_ZA_OFFSET_H | READ_FLAG, &data[0], 2);
	accel_bias_reg[2] = (int16_t) ((int16_t)data[0] << 8) | data[1];

	// Define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
	uint32_t mask = 1uL;
	// Define array to hold mask bit for each accelerometer bias axis
	uint8_t mask_bit[3] = {0, 0, 0};

	for (ii = 0; ii < 3; ii++) {
			// If temperature compensation bit is set, record that fact in mask_bit
			if (accel_bias_reg[ii] & mask) mask_bit[ii] = 0x01;
	}

	// Construct total accelerometer bias, including calculated average accelerometer bias from above
	// Subtract calculated averaged accelerometer bias scaled to 2048 LSB/g (16 g full scale)
	accel_bias_reg[0] -= (accel_bias[0]/8);
	accel_bias_reg[1] -= (accel_bias[1]/8);
	accel_bias_reg[2] -= (accel_bias[2]/8);

	data[0] = (accel_bias_reg[0] >> 8) & 0xFF;
	data[1] = (accel_bias_reg[0])      & 0xFF;
	// preserve temperature compensation bit when writing back to accelerometer bias registers
	data[1] = data[1] | mask_bit[0];
	data[2] = (accel_bias_reg[1] >> 8) & 0xFF;
	data[3] = (accel_bias_reg[1])      & 0xFF;
	// preserve temperature compensation bit when writing back to accelerometer bias registers
	data[3] = data[3] | mask_bit[1];
	data[4] = (accel_bias_reg[2] >> 8) & 0xFF;
	data[5] = (accel_bias_reg[2])      & 0xFF;
	// preserve temperature compensation bit when writing back to accelerometer bias registers
	data[5] = data[5] | mask_bit[2];

	// Output scaled accelerometer biases for manual subtraction in the main program
	dest2[0] = (float) accel_bias[0] / (float) accelsensitivity;
	dest2[1] = (float) accel_bias[1] / (float) accelsensitivity;
	dest2[2] = (float) accel_bias[2] / (float) accelsensitivity;
	
}

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	x_1 = 0;
	y_1 = 0;
	z_1 = 0;
	KModel kalmanModelX;
	//KModel kalmanModelY;
	//KModel kalmanModelZ;

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_SPI1_Init();

  /* USER CODE BEGIN 2 */
	
	// MPU needs to warm up
	HAL_Delay(100);

	// reset device, reset all registers, clear gyro and accelerometer bias registers
	writeByte(hspi1, MPUREG_PWR_MGMT_1, BIT_H_RESET);

	// do the handshake
	uint8_t whoami = readByte(hspi1, MPUREG_WHOAMI | READ_FLAG);

	if (whoami != 0x71) {
		// if not -- go to error state
		Error_Handler();
	}
	
	// Calibrate MPU
	mpuCalibrate(g_bias, a_bias);

	int16_t acc_in[3];
	float32_t acc_out[3];

	// initialize kalman
	kalman_init(&huart2, &kalmanModelX);
	//kalman_init(&huart2, &kalmanModelY);
	//kalman_init(&huart2, &kalmanModelZ);
	
	kalman_process(&kalmanModelX, 0.0493);
	
	char message[40];
		
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
//		readAcc(hspi1, acc_in);
//		adjustAcc(acc_in, acc_out);
//		kalman_process(&kalmanModelX, acc_out[0]);
//		//kalman_process(&kalmanModelY, acc_out[1]);
//		//kalman_process(&kalmanModelZ, acc_out[2]);
//		
//		sprintf(message, "%.3f,%.3f,\n", acc_out[0], kalmanModelX.MM.pData[2]);
//		debugMessage(&huart2, message);
		
		//HAL_Delay(1); // 1 ms

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 57600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin : PB6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
	uint8_t txBuffer[1];
	// 0x03 -- error code
	txBuffer[0] = 0x03;

	HAL_UART_Transmit(&huart2, txBuffer, 1, 100);

  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
