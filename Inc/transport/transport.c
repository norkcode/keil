#include "mpu.h"
#include "transport.h"

void transportEnable()
{
	// transport mode enabled
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
}

void transportDisable()
{
	// transport mode disabled
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
	// wait a bit
	HAL_Delay(10);
}
