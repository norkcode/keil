#include "sigma_points.h"



/**
 * Sigma points
 */
void sigma_points(
	const arm_matrix_instance_f32 *m,
	const arm_matrix_instance_f32 *P,
	const float32_t c,
	arm_matrix_instance_f32 *X)
{
	// A matrix
	arm_matrix_instance_f32 A;
	// nxn = 3x3 = 9
	float32_t A_f32[9] = {0};
	arm_mat_init_f32(&A, 3, 3, A_f32);
	
	arm_matrix_instance_f32 Pc;
	float32_t Pc_f32[9] = {0};
	// copy P data to Pc_f32
	arm_copy_f32(P->pData, Pc_f32, 9);
	
	arm_mat_init_f32(&Pc, 3, 3, Pc_f32);
	
	// Y matrix
	arm_matrix_instance_f32 Y;
	// m->numRows*m->numRows = 3x3 = 9
	float32_t Y_f32[9] = {0};
	// make m vector to matrix, there every column is the same m vector
	for (uint8_t s_i = 0; s_i < 3; s_i++) { // 3 = m->numRows
		for (uint8_t s_j = 0; s_j < 3; s_j++) { // 3 = m->numRows
			float32_t temp = m->pData[s_j];
			Y_f32[s_i+s_j] = temp;
		}
	}
	arm_mat_init_f32(&Y, m->numRows, m->numRows, Y_f32);
	
	// Y+A matrix
	arm_matrix_instance_f32 YplusA;
	float32_t YplusA_f32[9]; // P->numCols*P->numRows = 3x3 = 9
	arm_mat_init_f32(&YplusA, P->numRows, P->numCols, YplusA_f32);
	
	// Y-A matrix
	arm_matrix_instance_f32 YminusA;
	float32_t YminusA_f32[9]; // P->numCols*P->numRows = 3x3 = 9
	arm_mat_init_f32(&YminusA, P->numRows, P->numCols, YminusA_f32);
	
	// calculate cholesky
	cholesky(&Pc, 3, &Pc);

	arm_mat_trans_f32(&Pc, &Pc);
	arm_mat_scale_f32(&Pc, c, &A);
	
	// Y-A
	arm_mat_sub_f32(&Y, &A, &YminusA);
	// Y+A
	arm_mat_add_f32(&Y, &A, &YplusA);
	
	// first row is m
	for (uint8_t i=0; i < m->numRows; i++) {
		X->pData[i] = m->pData[i];
	}
	uint8_t offset = 1;
	// second m->row * m->row is YplusA
	for (uint8_t i = 0; i < P->numCols; i++) {
		for (uint8_t j = 0; j < P->numRows; j++) {
			uint8_t x_pdata_ind = offset+i+(j*7);
			uint8_t y_plus_a_ind = (j*3)+i;
			X->pData[x_pdata_ind] = YplusA.pData[y_plus_a_ind];
		}
	}
	// m->col*1 + P->col*P->row
	offset = 4;
	
	// third m->row*m->row*YplusA is YminusA
	for (uint8_t i = 0; i < P->numCols; i++) {
		for (uint8_t j = 0; j < P->numRows; j++) {
			X->pData[offset+i+(j*7)] = YminusA.pData[(j*3)+i];
		}
	}
}
