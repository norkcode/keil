#define ARM_MATH_CM4

#include "stm32f4xx_hal.h"
#include "arm_math.h"
#include "sigma_points.h"
#include "cholesky.h"
#include "matrix.h"
#include "ut.h"

//void kalman_init(UART_HandleTypeDef *huart2, KModel *kmodel);
//void kalman_init_x(UART_HandleTypeDef *huart2, KModel *m);
//void kalman_init_y(UART_HandleTypeDef *huart2, KModel *m);
void kalman_init(UART_HandleTypeDef *huart2, KModel *m);
void kalman_process(KModel * kmodel, float32_t sample);
