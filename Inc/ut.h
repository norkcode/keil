#define ARM_MATH_CM4

#include "stm32f4xx_hal.h"
#include "arm_math.h"
#include "kalman_model.h"

// Transform
void ut(
	const arm_matrix_instance_f32 *f,
	const arm_matrix_instance_f32 *X,
	const arm_matrix_instance_f32 *W,
	const arm_matrix_instance_f32 *R,
	arm_matrix_instance_f32 *y,
	arm_matrix_instance_f32 *Y,
	arm_matrix_instance_f32 *P,
	arm_matrix_instance_f32 *Y1
);
