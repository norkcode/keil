#include "stm32f4xx_hal.h"
#include "arm_math.h"

typedef struct KModel {
	UART_HandleTypeDef * huart2;
	// non linear map
	arm_matrix_instance_f32 f;
	// measurement equation
	arm_matrix_instance_f32 h;
	// sample rate
	float32_t dT;
	// number of states
	uint8_t n;
	// number of measurements
	uint8_t ss;
	// std of process
	float32_t q;
	// std of measurement
	float32_t r;
	// covariance of process
	float32_t Q_f32[9];
	arm_matrix_instance_f32 Q;
	// covariance of measurement
	float32_t R_f32[1];
	arm_matrix_instance_f32 R;
	// something 1
	uint8_t nx;
	// something 2
	uint8_t nz;
	// something 3
	float32_t m_f32[3];
	arm_matrix_instance_f32 m;
	// std of process in matrix form
	float32_t P_f32[9];
	arm_matrix_instance_f32 P;
	// something 4
	uint8_t kappa;
	// something 5
	float32_t W_f32[7];
	arm_matrix_instance_f32 W;
	// something 6
	float32_t c;
	// Rezults matrix
	float32_t MM_f32[3];
	arm_matrix_instance_f32 MM;
	// results covariance
	float32_t PP_f32[1];
	arm_matrix_instance_f32 PP;
	
} KModel;
