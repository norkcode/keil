#include "stm32f4xx_hal.h"

/* read accumulator */
void readAcc(SPI_HandleTypeDef, int16_t*);
/* read gyroscope */
void readGyro(SPI_HandleTypeDef, int16_t*);
/* read one byte */
uint8_t readByte(SPI_HandleTypeDef, uint8_t);
/* read a collection of bytes */
void readBytes(SPI_HandleTypeDef, uint8_t, uint8_t *, int8_t);
/* read fifo package count */
uint8_t readFifoPackageCount(SPI_HandleTypeDef hspi1);
