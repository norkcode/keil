#include "cholesky.h"

void cholesky(
  arm_matrix_instance_f32 *A,
	const uint8_t n,
	arm_matrix_instance_f32 * L) 
{
	uint8_t col_i = 0;
	uint8_t col_j = 0;
	uint8_t col_k = 0;
	float32_t col_s = 0.0f;
	uint8_t col_j_to;
	uint8_t col_iind;
	uint8_t col_jind;
	uint8_t col_pDataInd;
	uint8_t col_ind;
	float32_t col_variable;
	float32_t col_result;
	
	for (col_i = 0; col_i < n; col_i++) {
		col_j_to = col_i+1;
		for (col_j = 0; col_j < col_j_to; col_j++) {
			col_s = 0.0f;
			for (col_k = 0; col_k < col_j; col_k++) {
				col_iind = col_i*n+col_k;
				col_jind = col_j*n+col_k;
				col_s += L->pData[col_iind] * L->pData[col_jind];
			} 
			col_pDataInd = col_i*n+col_j;
			if (col_i == col_j) {
				col_ind = col_i*n+col_i;
				col_variable = A->pData[col_ind];
				col_variable = col_variable-col_s;
				arm_sqrt_f32(col_variable, &col_result);
				L->pData[col_pDataInd] = col_result;
			} else {
				uint8_t col_jind = col_j*n+col_j;
				col_result = (1.0f/L->pData[col_jind]*(A->pData[col_pDataInd]-col_s));
				L->pData[col_pDataInd] = col_result;
			}
		}
	}
}
